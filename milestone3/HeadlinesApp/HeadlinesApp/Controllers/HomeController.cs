﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HeadlinesApp.Models;
using System.Collections;

namespace HeadlinesApp.Controllers
{

    public class HomeController : Controller
    {
        List<HeadlineModel> headlines = new List<HeadlineModel>();

        public HomeController()
        {
            headlines.Add(new HeadlineModel("Donald Trump wins the election", 33, new DateTime(2016, 7, 28, 12, 45, 55)));
            headlines.Add(new HeadlineModel("Cat eats dog for breakfast.", 25, new DateTime(2016, 8, 23, 7, 45, 25)));
            headlines.Add(new HeadlineModel("Apple changes name to Pear.", 10, new DateTime(2016, 2, 28, 2, 45, 35)));
            headlines.Add(new HeadlineModel("Microsoft ships good software.", 0, new DateTime(2016, 3, 20, 12, 45, 45)));
            headlines.Add(new HeadlineModel("Viagra overdose leads to all nighter.", 3, new DateTime(2016, 4, 12, 4, 45, 55)));
            headlines.Add(new HeadlineModel("US changes to metric system.", 13, new DateTime(2016, 6, 28, 3, 55, 15)));
            headlines.Add(new HeadlineModel("Coke buys Pepsi, says they make a better product.", 8, new DateTime(2016, 1, 8, 12, 5, 25)));
            headlines.Add(new HeadlineModel("Self driving car out on a drive in date.", 9, new DateTime(2016, 7, 8, 12, 45, 5)));
            headlines.Add(new HeadlineModel("One weird trick to losing weight.  Don't eat.", 4, new DateTime(2016, 4, 3, 1, 10, 1)));

        }

        
        public ActionResult Index()
        {
            return View(headlines);
        }

       

    }
}